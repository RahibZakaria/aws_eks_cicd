# Your VPC must have DNS hostname and DNS resolution support. Otherwise, your nodes cannot register with your cluster.
resource "aws_vpc" "eks_vpc" {
  cidr_block                       = "10.0.0.0/16"
  instance_tenancy                 = "default"
  enable_dns_hostnames             = true
  enable_dns_support               = true
  assign_generated_ipv6_cidr_block = false

  tags = {
    Name = "EKS-VPC"
  }
}

# Internet gateway
resource "aws_internet_gateway" "igw" {
  vpc_id = aws_vpc.eks_vpc.id

  tags = {
    Name = "EKS-IGW"
  }
}

# Subnets

# Public subnet
resource "aws_subnet" "pub-1" {
  vpc_id                  = aws_vpc.eks_vpc.id
  cidr_block              = "10.0.1.0/24"
  availability_zone       = "us-east-1a"
  map_public_ip_on_launch = true
  # every new instance that will be deployed in the public
  # subnet will automatically get public ip address

  tags = {
    Name                        = "public-us-east-1a"
    "kubernetes.io/cluster/eks" = "shared"
    "kubernetes.io/role/elb"    = 1
  }
}

resource "aws_subnet" "pub-2" {
  vpc_id                  = aws_vpc.eks_vpc.id
  cidr_block              = "10.0.2.0/24"
  availability_zone       = "us-east-1b"
  map_public_ip_on_launch = true

  tags = {
    Name                        = "public-us-east-1b"
    "kubernetes.io/cluster/eks" = "shared"
    "kubernetes.io/role/elb"    = 1
  }
}

# private subnet
resource "aws_subnet" "pvt-1" {
  vpc_id            = aws_vpc.eks_vpc.id
  cidr_block        = "10.0.3.0/24"
  availability_zone = "us-east-1a"

  tags = {
    Name                        = "private-us-east-1a"
    "kubernetes.io/cluster/eks" = "shared"
    "kubernetes.io/role/internal-elb"    = 1
  }
}

resource "aws_subnet" "pvt-2" {
  vpc_id            = aws_vpc.eks_vpc.id
  cidr_block        = "10.0.4.0/24"
  availability_zone = "us-east-1b"

  tags = {
    Name                        = "private-us-east-1b"
    "kubernetes.io/cluster/eks" = "shared"
    "kubernetes.io/role/internal-elb"    = 1
  }
}

# EIPs
resource "aws_eip" "nat1" {
  depends_on = [aws_internet_gateway.igw]
}

resource "aws_eip" "nat2" {
  depends_on = [aws_internet_gateway.igw]
}

# NAT GW
resource "aws_nat_gateway" "gw1" {
  # Nat gw translates the private ip address to public ip address to get internet access
  allocation_id = aws_eip.nat1.id
  subnet_id     = aws_subnet.pub-1.id

  tags = {
    Name = "NAT 1"
  }
}

resource "aws_nat_gateway" "gw2" {
  # Nat gw translates the private ip address to public ip address to get internet access
  allocation_id = aws_eip.nat2.id
  subnet_id     = aws_subnet.pub-2.id

  tags = {
    Name = "NAT 2"
  }
}

# Route tables
# public route table
resource "aws_route_table" "public-rt" {
  vpc_id = aws_vpc.eks_vpc.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.igw.id
  }

  tags = {
    Name = "EKS-public-rt"
  }
}

resource "aws_route_table_association" "public-1" {
  route_table_id = aws_route_table.public-rt.id
  subnet_id      = aws_subnet.pub-1.id
}

resource "aws_route_table_association" "public-2" {
  route_table_id = aws_route_table.public-rt.id
  subnet_id      = aws_subnet.pub-2.id
}

# Private rt 1
resource "aws_route_table" "pvt-rt-1" {
  vpc_id = aws_vpc.eks_vpc.id

  route {
    cidr_block     = "0.0.0.0/0"
    nat_gateway_id = aws_nat_gateway.gw1.id
  }

  tags = {
    Name = "eks-pvt-rt-1"
  }
}

resource "aws_route_table_association" "pvt-1" {
  route_table_id = aws_route_table.pvt-rt-1.id
  # Subnet id to create an association
  subnet_id = aws_subnet.pvt-1.id
}

resource "aws_route_table" "pvt-rt-2" {
  vpc_id = aws_vpc.eks_vpc.id

  route {
    cidr_block     = "0.0.0.0/0"
    nat_gateway_id = aws_nat_gateway.gw2.id
  }

  tags = {
    Name = "eks-pvt-rt-2"
  }
}

resource "aws_route_table_association" "pvt-2" {
  route_table_id = aws_route_table.pvt-rt-2.id
  # Subnet id to create an association
  subnet_id = aws_subnet.pvt-2.id
}