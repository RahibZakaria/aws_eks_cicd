terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "5.17.0"
    }
  }
  backend "s3" {
        bucket = "personal-aws-terraform-projects"
        key    = "aws_eks_cicd_project/main/eks_infra.tfstate"
        region = "us-east-1"
  }
}

provider "aws" {
  region = "us-east-1"
  default_tags {
    tags = {
      ManagedBy = "Terraform"
      region    = "us-east-1"
    }
  }
}