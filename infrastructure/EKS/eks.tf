resource "aws_iam_role" "eks_cluster" {
  name = "eks-cluster"
  description = "AWS EKS will use this role to create AWS resources for Kubernetes clusters"
  # The policy that grants an entity permission to assume the role
  # Used to access AWS resources that you might not normally have access to
  # The role that Amazon EKS will use to create AWS resources for Kubernetes cluster
  assume_role_policy = <<POLICY
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "Service": "eks.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
POLICY
}

resource "aws_iam_role_policy_attachment" "eks_cluster_policy" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEKSClusterPolicy"
  role       = aws_iam_role.eks_cluster.name
}

resource "aws_eks_cluster" "eks_cluster" {
  name     = "eks"
  # kubernetes control plane to make calls to AWS API operations on your behalf
  role_arn = aws_iam_role.eks_cluster.arn

  # Desired Kubernetes cluster master version
  #version = "1.18"

  vpc_config {
    # Indicates whether or not the EKS private api server endpoint is enabled
    # e.g accessing EKS through bastion host or VPN
    endpoint_private_access = false

    # Indicates whether or not the EKS public API server endpoint is enabled
    endpoint_public_access = true

    # Must be at least two different availability zones
    subnet_ids = [
      aws_subnet.pub-1.id,
      aws_subnet.pub-2.id,
      aws_subnet.pvt-1.id,
      aws_subnet.pvt-2.id
    ]
  }

  depends_on = [aws_iam_role_policy_attachment.eks_cluster_policy]
}

# node group
resource "aws_iam_role" "nodes_general" {
  name = "eks-node-group-general"
  assume_role_policy = <<POLICY
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "Service": "ec2.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
POLICY
}

resource "aws_iam_role_policy_attachment" "eks_worker_node_policy_general" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEKSWorkerNodePolicy"
  role       = aws_iam_role.nodes_general.name
}

resource "aws_iam_role_policy_attachment" "eks_cni_policy_general" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEKS_CNI_Policy"
  role       = aws_iam_role.nodes_general.name
}

resource "aws_iam_role_policy_attachment" "eks_ec2_container_registry_read_only" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEC2ContainerRegistryReadOnly"
  role       = aws_iam_role.nodes_general.name
}

resource "aws_eks_node_group" "nodes_general" {
  cluster_name  = aws_eks_cluster.eks_cluster.name
  node_group_name = "nodes-general"
  node_role_arn = aws_iam_role.nodes_general.arn

  # identifiers of EC2 subnets to associate with EKS Node Group.
  # These subnets must have the following resource tag: kubernetes.io/cluster/CLUSTER_NAME
  # (where CLUSTER_NAME is replaced with the name of the EKS Cluster).
  subnet_ids    = [
    aws_subnet.pvt-1.id,
    aws_subnet.pvt-2.id
  ]

  # Configuration block with scaling settings
  scaling_config {
    desired_size = 1 # Desired number of worker nodes
    max_size     = 1 # Maximum number of worker nodes
    min_size     = 1 # Minimum number of worker nodes
  }

  # Type of Amazon Machine Image (AMI) associated with the EKS Node Group.
  # Valid values: AL2_x86_64, AL2_x86_64_GPU, AL2_ARM_64
  ami_type = "AL2_x86_64"

  # Type of capacity associated with the EKS Node Group
  # Valid values: ON_DEMAND, SPOT
  capacity_type = "ON_DEMAND"

  # Disk size in GiB for worker nodes
  disk_size = 20

  # Force version update if existing pods are unable to be drained due to a pod
  force_update_version = false

  # List of instance types associated with the EKS Node Group
  instance_types = ["t3.small"]

  labels = {
    role = "nodes-general"
  }

  # Kubernetes version
  #version = "1.18"

  depends_on = [
    aws_iam_role_policy_attachment.eks_worker_node_policy_general,
    aws_iam_role_policy_attachment.eks_cni_policy_general,
    aws_iam_role_policy_attachment.eks_ec2_container_registry_read_only
  ]
}