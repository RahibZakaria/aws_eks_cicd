resource "aws_ecr_repository" "docker_image_repo" {
  name = "aws_eks_cicd_project"
  image_tag_mutability = "MUTABLE"

  image_scanning_configuration {
    scan_on_push = true
  }
}
