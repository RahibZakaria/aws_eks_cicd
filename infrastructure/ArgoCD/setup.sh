#!/bin/bash

aws --version
#aws sts get-caller-identity #To check aws credentials
uname -m # To check architecture
echo "Installing and configuring kubectl..."
curl -LO "https://dl.k8s.io/release/$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/linux/amd64/kubectl"
install -o root -g root -m 0755 kubectl /usr/local/bin/kubectl
chmod +x kubectl
mkdir -p ~/.local/bin
mv ./kubectl ~/.local/bin/kubectl
aws eks update-kubeconfig --region us-east-1 --name eks
kubectl get namespaces
kubectl version