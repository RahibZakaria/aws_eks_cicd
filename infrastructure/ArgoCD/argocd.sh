#!/bin/bash

echo "Integrating ArgoCD..."
curl -sSL -o /usr/local/bin/argocd https://github.com/argoproj/argo-cd/releases/latest/download/argocd-linux-amd64
chmod +x /usr/local/bin/argocd
kubectl create namespace argocd
echo "Creating ArgoCD objects..."
kubectl apply -n argocd -f https://raw.githubusercontent.com/argoproj/argo-cd/stable/manifests/install.yaml
echo "Retrieving ArgoCD pods..."
kubectl get pods -n argocd
echo "Making ArgoCD publicly accessible..."
sleep 2
kubectl patch svc argocd-server -n argocd -p '{"spec": {"type": "LoadBalancer"}}'
echo "Setting up ArgoCD for syncing git manifests..."
sleep 2
kubectl get svc -n argocd
echo "Creating ArgoCD application..."
kubectl apply -f argocd_sync_def.yaml