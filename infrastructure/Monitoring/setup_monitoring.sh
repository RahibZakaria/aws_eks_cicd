echo "Installing Helm..."
curl -fsSL -o get_helm.sh https://raw.githubusercontent.com/helm/helm/main/scripts/get-helm-3
chmod 700 get_helm.sh
./get_helm.sh
echo "Installing Prometheus..."
helm repo add prometheus-community https://prometheus-community.github.io/helm-charts
helm repo update
#helm uninstall prometheus
helm install prometheus prometheus-community/prometheus
kubectl get svc
kubectl get deploy
kubectl get pods
echo "Installing Grafana..."
helm repo add grafana https://grafana.github.io/helm-charts
helm install grafana grafana/grafana
kubectl get svc
kubectl get deploy
kubectl get pods