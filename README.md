# AWS_EKS_CICD

## This is a project currently being worked on
The goal of the project is to create production grade Kubernetes infrastructure managed by terraform, applying gitops practices using ArgoCD and monitoring cluster using Prometheus and Grafana


## Architecture
![diagram](/diagram/architecture.jpeg)

## Completed:
#### Terraform code to maintain EKS infrastructure
#### terraform statefile will be stored in AWS S3 bucket
#### Entire development process automation using Gitlab CI


## TODO:
#### Simple custom app deployment for testing
#### CD part will be managed by ArgoCD
#### Prometheus and Grafana integration for cluster monitoring